## Build From Source
This folder contains guides form Indy-Sdk (the predecessor to VDR-Tools) which 
contain instructions on how to build from source for various platforms. Evernym 
does not guarantee support for built artifacts, but as of version 0.8.0 these 
guides should still work, possibly with minor changes. 