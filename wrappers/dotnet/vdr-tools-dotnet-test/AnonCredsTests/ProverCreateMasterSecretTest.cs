﻿using Com.Evernym.Vdrtools.AnonCredsApi;
using Com.Evernym.Vdrtools.Test.Util;
using Com.Evernym.Vdrtools.WalletApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Com.Evernym.Vdrtools.Test.AnonCredsTests
{
    [TestClass]
    public class ProverCreateMasterSecretTest : AnonCredsIntegrationTestBase
    {
        [TestMethod]
        public async Task TestProverCreateMasterSecretWorks()
        {
        }

        [TestMethod] 
        public async Task TestProverCreateMasterSecretWorksForDuplicate()
        {
            var ex = await Assert.ThrowsExceptionAsync<DuplicateMasterSecretNameException>(() =>
               AnonCreds.ProverCreateMasterSecretAsync(wallet, masterSecretId)
           );
        }

        [TestMethod]
        public async Task TestProverCreateMasterSecretWorksForEmptyName()
        {
            await AnonCreds.ProverCreateMasterSecretAsync(wallet, null);
        }
    }
}
