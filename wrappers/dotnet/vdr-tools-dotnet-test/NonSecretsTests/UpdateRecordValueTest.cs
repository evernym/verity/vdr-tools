﻿using Com.Evernym.Vdrtools.NonSecretsApi;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Com.Evernym.Vdrtools.WalletApi;

namespace Com.Evernym.Vdrtools.Test.NonSecretsTests
{
    [TestClass]
    public class UpdateRecordValueTest : NonSecretsIntegrationTestBase
    {
        [TestMethod]
        public async Task TestUpdateRecordValueWorks()
        {
            await NonSecrets.AddRecordAsync(wallet, type, id, value, tagsEmpty);

            await CheckRecordFieldAsync(wallet, type, id, "value", value);

            await NonSecrets.UpdateRecordValueAsync(wallet, type, id, value2);

            await CheckRecordFieldAsync(wallet, type, id, "value", value2);

        }

        [TestMethod]
        public async Task TestUpdateRecordValueWorksForNotFoundRecord()
        {
            var ex = await Assert.ThrowsExceptionAsync<WalletItemNotFoundException>(() =>
                NonSecrets.UpdateRecordValueAsync(wallet, type, id, value)
            );
        }
    }
}
