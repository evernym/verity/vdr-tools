﻿using Com.Evernym.Vdrtools.Test.PoolTests;

namespace Com.Evernym.Vdrtools.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var test = new CreatePoolTest();
            test.TestCreatePoolWorksForNullConfig().Wait();
        }
    }

}
