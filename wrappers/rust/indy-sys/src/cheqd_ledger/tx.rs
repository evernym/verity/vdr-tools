use crate::{CString, BString, CommandHandle, Error};
use crate::ResponseStringCB;

extern "C" {
    pub fn cheqd_ledger_tx_build_query_get_tx_by_hash(
        command_handle: CommandHandle,
        hash: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_cheqd_parse_query_get_tx_by_hash_resp(
        command_handle: CommandHandle,
        query_resp: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_tx_build_query_simulate(
        command_handle: CommandHandle,
        tx: BString,
        tx_len: u32,
        cb: Option<ResponseStringCB>,
    ) -> Error;

    pub fn cheqd_ledger_tx_parse_query_simulate_resp(
        command_handle: CommandHandle,
        query_resp: CString,
        cb: Option<ResponseStringCB>,
    ) -> Error;
}
