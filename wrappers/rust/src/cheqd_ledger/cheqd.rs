use crate::{ErrorCode, IndyError};

use std::ffi::CString;

use futures::Future;

use crate::ffi::cheqd_ledger;
use crate::ffi::{ResponseStringCB, ResponseSliceCB, WalletHandle};

use crate::utils::callbacks::{ClosureHandler, ResultHandler};
use crate::CommandHandle;

pub fn sign_msg_write_request(
    wallet_handle: WalletHandle,
    fully_did: &str,
    request_bytes: &[u8],
) -> Box<dyn Future<Item = Vec<u8>, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_slice();

    let err = _sign_msg_write_request(command_handle, wallet_handle, fully_did, request_bytes,  cb);

    ResultHandler::slice(command_handle, err, receiver)
}

pub fn _sign_msg_write_request(
    command_handle: CommandHandle,
    wallet_handle: WalletHandle,
    fully_did: &str,
    request_bytes: &[u8],
    cb: Option<ResponseSliceCB>,
) -> ErrorCode {
    let fully_did = c_str!(fully_did);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_sign_msg_write_request(
            command_handle,
            wallet_handle,
            fully_did.as_ptr(),
            request_bytes.as_ptr() as *const u8,
            request_bytes.len() as u32,
            cb,
        )
    })
}

pub fn build_msg_create_did(
    did: &str,
    verkey: &str,
) -> Box<dyn Future<Item = Vec<u8>, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_slice();

    let err = _build_msg_create_did(command_handle, did, verkey, cb);

    ResultHandler::slice(command_handle, err, receiver)
}

fn _build_msg_create_did(
    command_handle: CommandHandle,
    did: &str,
    verkey: &str,
    cb: Option<ResponseSliceCB>,
) -> ErrorCode {
    let did = c_str!(did);
    let verkey = c_str!(verkey);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_build_msg_create_did(
            command_handle,
            did.as_ptr(),
            verkey.as_ptr(),
            cb,
        )
    })
}

pub fn parse_msg_create_did_resp(
    commit_resp: &str,
) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _parse_msg_create_did_resp(command_handle, commit_resp, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _parse_msg_create_did_resp(
    command_handle: CommandHandle,
    commit_resp: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let commit_resp = c_str!(commit_resp);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_parse_msg_create_did_resp(
            command_handle,
            commit_resp.as_ptr(),
            cb,
        )
    })
}

pub fn build_msg_update_did(
    did: &str,
    verkey: &str,
    version_id: &str,
) -> Box<dyn Future<Item = Vec<u8>, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_slice();

    let err = _build_msg_update_did(command_handle, did, verkey, version_id, cb);

    ResultHandler::slice(command_handle, err, receiver)
}

fn _build_msg_update_did(
    command_handle: CommandHandle,
    did: &str,
    verkey: &str,
    version_id: &str,
    cb: Option<ResponseSliceCB>,
) -> ErrorCode {
    let did = c_str!(did);
    let verkey = c_str!(verkey);
    let version_id = c_str!(version_id);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_build_msg_update_did(
            command_handle,
            did.as_ptr(),
            verkey.as_ptr(),
            version_id.as_ptr(),
            cb,
        )
    })
}

pub fn parse_msg_update_did_resp(
    commit_resp: &str,
) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _parse_msg_update_did_resp(command_handle, commit_resp, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _parse_msg_update_did_resp(
    command_handle: CommandHandle,
    commit_resp: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let commit_resp = c_str!(commit_resp);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_parse_msg_update_did_resp(
            command_handle,
            commit_resp.as_ptr(),
            cb,
        )
    })
}

pub fn build_query_get_did(did: &str) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _build_query_get_did(command_handle, did, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _build_query_get_did(
    command_handle: CommandHandle,
    did: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let did = c_str!(did);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_build_query_get_did(command_handle, did.as_ptr(), cb)
    })
}

pub fn parse_query_get_did_resp(
    query_resp: &str,
) -> Box<dyn Future<Item = String, Error = IndyError>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _parse_query_get_did_resp(command_handle, query_resp, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _parse_query_get_did_resp(
    command_handle: CommandHandle,
    query_resp: &str,
    cb: Option<ResponseStringCB>,
) -> ErrorCode {
    let query_resp = c_str!(query_resp);

    ErrorCode::from(unsafe {
        cheqd_ledger::cheqd::cheqd_ledger_cheqd_parse_query_get_did_resp(
            command_handle,
            query_resp.as_ptr(),
            cb,
        )
    })
}
