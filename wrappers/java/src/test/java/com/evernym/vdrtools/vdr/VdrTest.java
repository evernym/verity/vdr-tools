package com.evernym.vdrtools.vdr;

import com.evernym.vdrtools.IndyException;
import com.evernym.vdrtools.IndyIntegrationTestWithSingleWallet;
import com.evernym.vdrtools.did.Did;
import com.evernym.vdrtools.did.DidResults;
import com.evernym.vdrtools.vdr.VdrResults.PingResult;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import java.util.*;

import static com.evernym.vdrtools.utils.PoolUtils.buildGenesisTransactionsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class VdrTest extends IndyIntegrationTestWithSingleWallet {

    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger("VdrTestLogger");

    @Test
    public void testVdrOpenIdnyAndCheqdPools() throws Exception, IndyException {
        VdrBuilder builder = VdrBuilder.create();
        assertNotNull(builder);
        VdrParams.TaaConfig tc = new VdrParams.TaaConfig("a0ab0aada", "on_file", 1L); // for testing DTO processing
        builder.registerIndyLedger(INDY_NAMESPACES_LIST, buildGenesisTransactionsString(), tc).get();
        VDR vdr = builder.build();
        assertNotNull(vdr);

        JSONObject didJson = new JSONObject();
        didJson.put("seed", TRUSTEE_SEED);
        didJson.put("method_name", INDY_NAMESPACE);
        DidResults.CreateAndStoreMyDidResult result = Did.createAndStoreMyDid(wallet, didJson.toString()).get();

        List<String> namespacesList = Collections.singletonList(INDY_NAMESPACE);

        Map<String, PingResult> pingResult = vdr.ping(namespacesList).get();

        long countOfUnavailableNamespaces = pingResult.entrySet()
                .stream()
                .filter(entry -> !entry.getValue().isSuccessful())
                .peek(entry -> logger.info("unavailable namespace '{}', code - {}, message - {}",
                        entry.getKey(), entry.getValue().getCode(), entry.getValue().getMessage())).count();
        assertEquals("There are unavailable namespaces", 0, countOfUnavailableNamespaces);

        ArrayList<String> didDocs = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String res = vdr.resolveDID(result.getDid()).get();
            didDocs.add(res);
        }

        JSONObject didDoc = new JSONObject(didDocs.get(0));

        assertEquals(DID_TRUSTEE, didDoc.getString("did"));

        vdr.cleanup().get();
    }
}
