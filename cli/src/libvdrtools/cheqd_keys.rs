use vdrtools::IndyError;
use vdrtools::future::Future;
use vdrtools::{WalletHandle};
use vdrtools::cheqd_keys;

pub struct CheqdKeys {}

impl CheqdKeys {
    pub fn add_random(wallet_handle: WalletHandle, alias: &str) -> Result<String, IndyError> {
        cheqd_keys::add_random(wallet_handle, alias).wait()
    }

    pub fn add_from_mnemonic(wallet_handle: WalletHandle, alias: &str, mnemonic: &str, passphrase: &str,) -> Result<String, IndyError> {
        cheqd_keys::add_from_mnemonic(wallet_handle, alias, mnemonic, passphrase).wait()
    }

    pub fn get_info(wallet_handle: WalletHandle, alias: &str) -> Result<String, IndyError> {
        cheqd_keys::get_info(wallet_handle, alias).wait()
    }

    pub fn list(wallet_handle: WalletHandle) -> Result<String, IndyError> {
        cheqd_keys::list(wallet_handle).wait()
    }
}
