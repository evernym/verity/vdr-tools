use vdrtools::IndyError;
use vdrtools::future::Future;
use vdrtools::cheqd_ledger;
use vdrtools::WalletHandle;

pub struct CheqdLedger {}

impl CheqdLedger {
    pub fn parse_query_account_resp(query_resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::auth::parse_query_account_resp(query_resp).wait()
    }

    pub fn build_query_account(address: &str) -> Result<String, IndyError> {
        cheqd_ledger::auth::build_query_account(address).wait()
    }

    pub fn build_tx(pool_alias: &str,
                    sender_public_key: &str,
                    msg: &[u8],
                    account_number: u64,
                    sequence_number: u64,
                    max_gas: u64,
                    max_coin_amount: u64,
                    max_coin_denom: &str,
                    timeout_height: u64,
                    memo: &str) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::auth::build_tx(pool_alias, sender_public_key, msg, account_number, sequence_number, max_gas, max_coin_amount, max_coin_denom, timeout_height, memo).wait()
    }

    pub fn sign_tx(wallet_handle: WalletHandle,
                   alias: &str,
                   tx: &[u8]) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::auth::sign_tx(wallet_handle, alias, tx).wait()
    }

    pub fn sign_msg_write_request(wallet_handle: WalletHandle,
                                  did: &str,
                                  request_bytes: &[u8],) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::cheqd::sign_msg_write_request(wallet_handle, did, request_bytes).wait()
    }

    pub fn build_msg_create_did(did: &str,
                                verkey: &str) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::cheqd::build_msg_create_did(did, verkey).wait()
    }

    pub fn parse_msg_create_did_resp(commit_resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::cheqd::parse_msg_create_did_resp(commit_resp).wait()
    }

    pub fn build_msg_update_did(did: &str,
                                verkey: &str,
                                version_id: &str) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::cheqd::build_msg_update_did(did, verkey, version_id).wait()
    }

    pub fn parse_msg_update_did_resp(commit_resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::cheqd::parse_msg_update_did_resp(commit_resp).wait()
    }

    pub fn build_msg_send(from: &str,
                          to: &str,
                          amount: &str,
                          denom: &str) -> Result<Vec<u8>, IndyError> {
        cheqd_ledger::bank::build_msg_send(from, to, amount, denom).wait()
    }

    pub fn parse_msg_send_resp(resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::bank::parse_msg_send_resp(resp).wait()
    }

    pub fn build_query_balance(address: &str,
                               denom: &str) -> Result<String, IndyError> {
        cheqd_ledger::bank::build_query_balance(address, denom).wait()
    }

    pub fn parse_query_balance_resp(resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::bank::parse_query_balance_resp(resp).wait()
    }

    pub fn build_query_get_did(did: &str) -> Result<String, IndyError> {
        cheqd_ledger::cheqd::build_query_get_did(did).wait()
    }

    pub fn parse_query_get_did_resp(query_resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::cheqd::parse_query_get_did_resp(query_resp).wait()
    }

    pub fn build_query_simulate(tx: &[u8]) -> Result<String, IndyError> {
        cheqd_ledger::tx::build_query_simulate(tx).wait()
    }

    pub fn parse_query_simulate_resp(query_resp: &str) -> Result<String, IndyError> {
        cheqd_ledger::tx::parse_query_simulate_resp(query_resp).wait()
    }

}
